

ifeq ($(KERNEL_SRC_DIR),)
  $(error KERNEL_SRC_DIR not defined)
endif

ifeq ($(KERNEL_CFG_NAME),)
  $(error KERNEL_CFG_NAME not defined)
endif

ifeq ($(BOARD_DTB_FILE_NAME),)
  $(error BOARD_DTB_FILE_NAME not defined)
endif

KERNEL_IMAGE_FILE := $(KERNEL_SRC_DIR)/arch/arm64/boot/Image
KERNEL_IMAGE_LZ4 := $(KERNEL_SRC_DIR)/arch/arm64/boot/Image.lz4
KERNEL_CONFIG := $(KERNEL_SRC_DIR)/.config
KERNEL_DEFCONFIG := $(KERNEL_SRC_DIR)/arch/arm64/configs/$(KERNEL_CFG_NAME)
KERNEL_SRC_FILES := $(shell find $(KERNEL_SRC_DIR) -name "*.c" -or -name "*.h" -or -name "*.dts" -or -name "*.dtsi")

KERNEL_TOOLCHAIN_ABS := $(realpath prebuilts/gcc/$(HOST_PREBUILT_TAG)/aarch64/aarch64-linux-android-4.9/bin)
KERNEL_CROSS_TOOLCHAIN := $(KERNEL_TOOLCHAIN_ABS)/aarch64-linux-android-

ifeq ($(wildcard $(KERNEL_DEFCONFIG)),)
  $(error $(KERNEL_DEFCONFIG) does not exist)
endif

$(KERNEL_CONFIG): $(KERNEL_DEFCONFIG)
	make -C $(KERNEL_SRC_DIR) ARCH=arm64 CROSS_COMPILE=$(KERNEL_CROSS_TOOLCHAIN) $(KERNEL_CFG_NAME)

$(KERNEL_IMAGE_FILE): $(KERNEL_CONFIG) $(KERNEL_SRC_FILES)
	make -C $(KERNEL_SRC_DIR) ARCH=arm64 CROSS_COMPILE=$(KERNEL_CROSS_TOOLCHAIN) Image modules dtbs -j20

$(KERNEL_IMAGE_LZ4): $(KERNEL_IMAGE_FILE)
	lz4 $(KERNEL_IMAGE_FILE) $(KERNEL_IMAGE_LZ4)

$(PRODUCT_OUT)/kernel: $(KERNEL_IMAGE_LZ4)
	$(ACP) $< $(PRODUCT_OUT)/kernel


$(BOARD_VENDOR_KERNEL_MODULES): $(KERNEL_IMAGE_FILE)


################### DTB file #############################

BOARD_DTB_FILE := $(KERNEL_SRC_DIR)/arch/arm64/boot/dts/freescale/$(BOARD_DTB_FILE_NAME)

$(BOARD_DTB_FILE): $(KERNEL_BIN)

BOARD_MKBOOTIMG_ARGS += --second $(BOARD_DTB_FILE)
#INSTALLED_2NDBOOTLOADER_TARGET = $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/arch/$(TARGET_KERNEL_ARCH)/boot/dts/freescale/videray-frizzen.dtb

$(PRODUCT_OUT)/boot.img: $(BOARD_DTB_FILE)

#################################

.PHONY: clean_kernel

clean_kernel:
	rm -rf $(PRODUCT_OUT)/kernel $(KERNEL_IMAGE_FILE) $(KERNEL_IMAGE_LZ4) $(BOARD_DTB_FILE)
	make -C $(KERNEL_SRC_DIR) distclean

## flash boot.img
## sudo dd if=boot.img of=/dev/sdb1 ; sync