

ifeq ($(UBOOT_CFG_NAME),)
  $(error UBOOT_CFG_NAME not defined)
endif

ifeq ($(UBOOT_SRC_DIR),)
  $(error UBOOT_SRC_DIR not defined)
endif

UBOOT_CFG_FILE := $(UBOOT_SRC_DIR)/.config
UBOOT_SRC_FILES := $(shell find $(UBOOT_SRC_DIR) -name "*.c" -or -name "*.h" -or -name "*.dts" -or -name "*.dtsi")
UBOOT_IMAGE := $(UBOOT_SRC_DIR)/u-boot.bin
UBOOT_SPL_IMAGE := $(UBOOT_SRC_DIR)/spl/u-boot-spl.bin
UBOOT_DTB_IMAGE := $(UBOOT_SRC_DIR)/u-boot.dtb
UBOOT_NODTB_IMAGE := $(UBOOT_SRC_DIR)/u-boot-nodtb.bin
BOOTLOADER_IMG := vendor/nxp-opensource/imx-mkimage/iMX8M/flash.bin


BOOTLOADER_CROSS_TOOLCHAIN := $(realpath prebuilts/gcc/$(HOST_PREBUILT_TAG)/aarch64/aarch64-linux-android-4.9/bin)/aarch64-linux-android-

$(UBOOT_CFG_FILE):
	make -C $(UBOOT_SRC_DIR) ARCH=arm CROSS_COMPILE=$(BOOTLOADER_CROSS_TOOLCHAIN) $(UBOOT_CFG_NAME)

$(UBOOT_IMAGE): $(UBOOT_CFG_FILE) $(UBOOT_SRC_FILES)
	make -C $(UBOOT_SRC_DIR) ARCH=arm CROSS_COMPILE=$(BOOTLOADER_CROSS_TOOLCHAIN)

$(UBOOT_SPL_IMAGE): $(UBOOT_IMAGE)
$(UBOOT_DTB): $(UBOOT_IMAGE)
$(UBOOT_NODTB_IMAGE): $(UBOOT_IMAGE)


################### Arm Trusted Firmware (ATF) ##########################

ATF_TOOLCHAIN_ABS := $(realpath prebuilts/gcc/$(HOST_PREBUILT_TAG)/aarch64/aarch64-linux-android-4.9/bin)
ATF_CROSS_COMPILE := $(ATF_TOOLCHAIN_ABS)/aarch64-linux-android-

ATF_PLAT := imx8mq
ATF_BIN := vendor/nxp-opensource/imx-mkimage/iMX8M/bl31.bin

$(ATF_BIN):
	$(MAKE) -C $(IMX_PATH)/arm-trusted-firmware/ CROSS_COMPILE=$(ATF_CROSS_COMPILE) PLAT=$(ATF_PLAT) clean && \
	$(MAKE) -C $(IMX_PATH)/arm-trusted-firmware/ CROSS_COMPILE=$(ATF_CROSS_COMPILE) PLAT=$(ATF_PLAT) bl31 -B && \
	cp $(IMX_PATH)/arm-trusted-firmware/build/imx8mq/release/bl31.bin $(ATF_BIN)


#################### mkimage tool ##############################

UBOOT_MKIMAGE_TOOL := $(UBOOT_SRC_DIR)/tools/mkimage

$(UBOOT_MKIMAGE_TOOL): $(UBOOT_IMAGE)


#################### bootloader ##############################

$(BOOTLOADER_IMG): $(UBOOT_SPL_IMAGE) $(UBOOT_IMAGE) $(UBOOT_DTB) $(ATF_BIN) $(UBOOT_MKIMAGE_TOOL)
	cp vendor/nxp/fsl-proprietary/uboot-firmware/imx8m/lpddr4_pmu_train_1d_dmem.bin vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp vendor/nxp/fsl-proprietary/uboot-firmware/imx8m/lpddr4_pmu_train_1d_imem.bin vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp vendor/nxp/fsl-proprietary/uboot-firmware/imx8m/lpddr4_pmu_train_2d_dmem.bin vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp vendor/nxp/fsl-proprietary/uboot-firmware/imx8m/lpddr4_pmu_train_2d_imem.bin vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp $(UBOOT_IMAGE) vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp $(UBOOT_SPL_IMAGE) vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp $(UBOOT_DTB_IMAGE) vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp $(UBOOT_NODTB_IMAGE) vendor/nxp-opensource/imx-mkimage/iMX8M/
	cp $(UBOOT_MKIMAGE_TOOL) vendor/nxp-opensource/imx-mkimage/iMX8M/mkimage_uboot
	make -C vendor/nxp-opensource/imx-mkimage SOC=iMX8M dtbs=u-boot.dtb flash_spl_uboot


bootloader: $(PRODUCT_OUT)/bootloader

$(PRODUCT_OUT)/bootloader: $(BOOTLOADER_IMG)
	$(ACP) $(BOOTLOADER_IMG) $(PRODUCT_OUT)/bootloader

.PHONY: clean_bootloader
clean_bootloader:
	make -C $(UBOOT_SRC_DIR) ARCH=arm CROSS_COMPILE=$(BOOTLOADER_CROSS_TOOLCHAIN) distclean
	rm -rf $(PRODUCT_OUT)/bootloader

droid: bootloader
otapackage: bootloader

#### flash bootloader
### sudo dd if=bootloader of=/dev/sdb bs=1k seek=33 ; sync
